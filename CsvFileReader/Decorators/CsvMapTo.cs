﻿namespace CsvFileReader.Decorators
{
    using System;

    /// <summary>
    /// this decorator allows you to map properties to other defined columns
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CsvMapTo : Attribute
    {
        #region Public Properties
        public string Name { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// map a property to another defined column
        /// </summary>
        /// <param name="Key">is a <typeparamref name="string"/>. The name of the column in the csv to read to</param>
        public CsvMapTo(string key)
        {
            if (key == null || key.Equals(String.Empty)) throw new ArgumentNullException(nameof(key));
            this.Name = key;
        }
        #endregion
    }
}
