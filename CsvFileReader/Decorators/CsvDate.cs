﻿
namespace CsvFileReader.Decorators
{
    using System;
    using System.Globalization;

    /// <summary>
    /// this class is a decorator for applying date formats to date
    /// properties in the model specified
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CsvDate : Attribute
    {
        #region Private Properties
        private string Format { get; set; }
        private Boolean Try { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Attribute Decorator for determining CSV date formats
        /// </summary>
        /// <param name="format"></param>
        /// <remarks>
        /// strings with dashes or slashes are automatically recognized by c# so this decorator
        /// is intended to be used when none are present other wise you should not be using
        /// this decorator
        /// </remarks>
        public CsvDate(string format)
        {
            if (format == null || format.Equals(String.Empty)) throw new ArgumentNullException(nameof(format));
            this.Format = format;
            this.Try = false;
        }
        /// <summary>
        /// overload for attempting instead of explicitly attempting with exception
        /// </summary>
        /// <param name="format"></param>
        public CsvDate(string format, Boolean Try)
        {
            if (format == null || format.Equals(String.Empty)) throw new ArgumentNullException(nameof(format));
            this.Format = format;
            this.Try = Try;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// this resolves the format for input
        /// </summary>
        /// <param name="input">is a <typeparamref name="String"/>. The date value in string format</param>
        /// <returns></returns>
        public DateTime Resolve(String content)
        {
            if (content == null || content.Equals(String.Empty)) throw new ArgumentNullException(nameof(content));
            if (this.Try)
            {
                DateTime date = new DateTime();
                DateTime.TryParseExact(content, this.Format, null, DateTimeStyles.None, out date);
                return date;
            }
            else
            {
                return DateTime.ParseExact(content, this.Format, CultureInfo.InvariantCulture);
            }
        }
        #endregion
    }
}
