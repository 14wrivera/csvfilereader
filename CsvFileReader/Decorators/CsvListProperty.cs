﻿namespace CsvFileReader.Decorators
{
    using System;

    /// <summary>
    /// this class is a decorator for applying date formats to date
    /// properties in the model specified
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CsvListProperty : Attribute
    {
        #region Properties
        public string Name { get; private set; }
        public char Delimiter { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Attribute Decorator for Custom Types 
        /// </summary>
        /// <param name="Name">is a <typeparamref name="String"/>. The name of the property this column maps to in the custom object</param>
        /// <remarks>
        /// this decorator is needed when you are using User defined types and you need the data from a column to map
        /// to the property which is an object and its property.
        /// </remarks>
        public CsvListProperty(String Name)
        {
            if (Name == null || Name.Equals(String.Empty)) throw new ArgumentNullException(nameof(Name));
            this.Name = Name;
        }
        /// <summary>
        /// Attribute Decorator for Custom Types that use a list. Delimiter needs to be defined
        /// </summary>
        /// <param name="Name">is a <typeparamref name="String"/>.The name of the property this column maps to in the custom object</param>
        /// <param name="Delimiter">is a <typeparamref name="char"/>. THe delimiter</param>
        /// <remarks>
        /// this decorator is needed when you are using User defined types and you need the data from a column to map
        /// to the property which is an object and its property.
        /// </remarks>
        public CsvListProperty(string Name, char Delimiter)
        {
            if (Name == null || Name.Equals(String.Empty)) throw new ArgumentNullException(nameof(Name));
            if (Delimiter == null) throw new ArgumentNullException(nameof(Delimiter));
            this.Name = Name;
            this.Delimiter = Delimiter;
        }
        /// <summary>
        /// Attribute Decorator for a primitive list. Delimiter needs to be defined
        /// </summary>
        /// <param name="Delimiter">is a <typeparamref name="char"/>. THe delimiter</param>
        public CsvListProperty(char Delimiter)
        {
            if (Delimiter == null) throw new ArgumentNullException(nameof(Delimiter));
            this.Delimiter = Delimiter;
        }
        #endregion
    }
}
