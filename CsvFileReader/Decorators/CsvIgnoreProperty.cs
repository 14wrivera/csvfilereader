﻿namespace CsvFileReader.Decorators
{
    using System;

    /// <summary>
    /// This is a Decorator for ignoring properties
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CsvIgnoreProperty : Attribute
    {
        #region Constructors
        /// <summary>
        /// Default constructor. If this attribute is present
        /// then CsvFileReader will ignore it.
        /// </summary>
        public CsvIgnoreProperty() { }
        #endregion
    }

}
