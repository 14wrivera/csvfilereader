namespace CsvFileReader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.IO;

    using CsvFileReader.Decorators;
    using CsvFileReader.Exceptions;
    using CsvFileReader.Interfaces;
    using CsvFileReader.Interfaces.Models;

    /// <summary>
    /// This is the CSV reader for reading csv files
    /// </summary>
    public sealed class CsvReader:ICsvReader
    {
        #region Properties
        public Dictionary<int, String> HeaderRow { get; private set; }
        public IFileParser FileParser { get; private set; }
        public String Path { get; private set; }
        public List<string> DataRows { get; private set; }
        public IDataAttribute DataAttributeMapper { get; private set; }

        public string this[int index]
        {
            get { return this.DataRows[index]; }
            set { this.DataRows[index] = value; }
        }

        public Boolean HasRows
        {
            get
            {
                if (DataRows.Count > 0)
                    return true;
                return false;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// reader for reading csv files
        /// </summary>
        /// <param name="path">is a <typeparamref name="string"/>. the file path to read from</param>
        /// <param name="fileParser">is a <typeparamref name="IFileParse"/>. the parser to use</param>
        /// <param name="attribute">is a <typeparamref name="IDataAttrubyte"/>. the service for mapping types and setting values</param>
        public CsvReader(string path, IFileParser fileParser, IDataAttribute attribute)
        {
            if (path == null || path.Equals(String.Empty)) throw new ArgumentNullException(nameof(path));
            if (fileParser == null) throw new ArgumentNullException(nameof(fileParser));
            if (attribute == null) throw new ArgumentNullException(nameof(attribute));

            this.Path = path;
            this.FileParser = fileParser;
            this.DataAttributeMapper = attribute;
            this.HeaderRow = new Dictionary<int, String>();
            this.RetrieveData();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// retrieves all rows of data from csv 
        /// </summary>
        private void RetrieveData()
        {
            //get all lines from the file
            DataRows = File.ReadLines(this.Path).ToList();
            if (!this.HasRows)
                throw new CsvEmptyFileException("File has no Data", this.Path);
        }

        /// <summary>
        /// gets the header column mappings
        /// </summary>
        /// <typeparam name="T">The Type of data model</typeparam>
        /// <param name="line">the line to parse through</param>
        private void GetHeaders<T>(String line) where T : class
        {
            if (line == null || line.Equals(String.Empty)) throw new ArgumentNullException(nameof(line));

            Object obj = Activator.CreateInstance<T>();
            foreach (PropertyInfo info in obj.GetType().GetProperties())
            {
                //skip properties that are decorated with ignore
                CsvIgnoreProperty ignore = info.GetCustomAttributes(typeof(CsvIgnoreProperty), false).
                    Cast<CsvIgnoreProperty>().SingleOrDefault();
                if (ignore != null)
                {
                    continue;
                }

                string propertyName = info.Name;

                //retrieve property name or if CsvMapTo decorator is used then CsvMapTo name
                CsvMapTo attribute = info.GetCustomAttributes(typeof(CsvMapTo), false).Cast<CsvMapTo>().SingleOrDefault();
                if (attribute != null)
                {
                    propertyName = attribute.Name;
                }
                //throw exception if property doesn't match
                if (!AddHeader(line, info.Name, propertyName))
                {
                    throw new CsvPropertyNotFoundException("Property " + info.Name + " of " +
                        typeof(T).ToString() + " not found in CSV",
                        this.Path, new Exception("All properties must by publically accessible and be present in the csv"));
                }
            }
        }
        /// <summary>
        /// searches for a column labeled as a property and adds it to our header dictionary if found
        /// </summary>
        /// <param name="line">is a<typeparamref name="String"/>. The actual row of data as a string</param>
        /// <param name="propertyName">is a<typeparamref name="String"/>.The name of the property</param>
        /// <param name="mappedName">is a<typeparamref name="String"/>.The mapped name</param>
        /// <returns>Weather the column was located</returns>
        private bool AddHeader(String line, String propertyName, String mappedName)
        {
            if (line == null||line.Equals(String.Empty)) throw new ArgumentNullException(nameof(line));
            if (propertyName == null||propertyName.Equals(String.Empty)) throw new ArgumentNullException(nameof(propertyName));
            if (mappedName == null ||mappedName.Equals(string.Empty)) throw new ArgumentNullException(nameof(mappedName));

            int i = 0;
            bool found = false;
            //pull out each column and check it against the property name
            foreach (String cell in this.FileParser.ParseRow(line))
            {
                // verify type property exists in the data, if this does not match 
                //then its possible that the header column is data
                if (mappedName == cell.Trim())
                {
                    this.HeaderRow.Add(i, propertyName);
                    found = true;
                }
                i++;
            }
            return found;
        }
        /// <summary>
        /// pops the first row of data and returns it
        /// </summary>
        /// <returns>the row of data</returns>
        private string RetrieveHeaderRow()
        {
            //pop of the first line and search for headers. 
            //These headers must match the property names of the type 
            String Headerdata = this.DataRows.FirstOrDefault();
            this.DataRows.RemoveAt(0);

            //throw exception if there is no data in the file after removing header row
            if (this.DataRows.Count == 0)
                throw new CsvEmptyRowException("No Data After Reading Headers", this.Path);

            return Headerdata;
        }

        /// <summary>
        /// this method implements the data retrieval portion after headers are 
        /// defined
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <returns>Enumerable objects of the type</returns>
        private IEnumerable<T> MapData<T>() where T : class
        {
            //instantiate container for data objects
            List<T> objects = new List<T>();

            int RowCount = 0;
            foreach (String line in this.DataRows)
            {
                //instantiate new object
                T obj = (T)Activator.CreateInstance<T>();

                //get the data from the line
                List<String> Line = this.FileParser.ParseRow(line);

                //check if the data has less than the amount as headers expects if not then we have a 
                //potential issue with the file. It may be the case that we have more data then headers 
                //because our model is only seeking partial data so this is not an issue;
                if (Line.Count < this.HeaderRow.Count)
                    throw new CsvEmptyColumnException("Data mismatch for " + typeof(T).ToString() + ". Row " +
                        RowCount + " is missing data column for a Property.", this.Path);

                this.MapColumn<T>(obj, Line);
                RowCount++;
                objects.Add(obj);
            }
            return objects as IEnumerable<T>;
        }
        /// <summary>
        /// This actually maps the columns
        /// </summary>
        /// <typeparam name="T">The Type</typeparam>
        /// <param name="obj">the Object that we are mapping to</param>
        /// <param name="Line">the line of parsed csv data</param>
        private void MapColumn<T>(T obj, List<String> Line) where T : class
        { 
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            if (Line == null) throw new ArgumentNullException(nameof(Line));
            int i = 0;
            //parse each cell and map columns to data
            foreach (String contents in Line)
            {
                //check if key exists, keys may not exist if properties are not all defined from csv
                //since the key maps to the index of the row contents
                if (this.HeaderRow.ContainsKey(i))
                {
                    String column = this.HeaderRow[i];
                    try
                    {
                        this.DataAttributeMapper.Resolve(obj, column, contents);
                    }
                    catch (Exception exc)
                    {
                        throw new CsvAttributeException(
                            "There was problem with setting the " + column +
                            " Property. Check the inner exception for more details", this.Path, i, exc);
                    }
                }
                i++;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// return Enumerable objects of a type for each record in the csv.
        /// first it pulls out the headers from the first row of the csv and maps them to the class
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <returns>Enumerable objects of the type</returns>
        public IEnumerable<T> RetrieveData<T>() where T : class
        {
            //pop off the first line and search for headers. 
            //These headers must match the property names of the type 
            String Headerdata = this.RetrieveHeaderRow();

            //get the headers for the first line
            this.GetHeaders<T>(Headerdata);

            return this.MapData<T>();
        }

        /// <summary>
        /// this method allows for custom mapping to csv files and removes header row
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <param name="headers">the definition mapping for the columns to class</param>
        /// <returns>Enumerable objects of the type</returns>
        public IEnumerable<T> RetrieveDataRemoveFirstRow<T>(Dictionary<int, string> headers) where T : class
        {
            if (headers == null) throw new ArgumentNullException(nameof(headers));

            //pop off the first line and search for headers. 
            this.RetrieveHeaderRow();

            //assign newly mapped data
            this.HeaderRow = headers;

            return this.MapData<T>();
        }

        /// <summary>
        /// this method allows for custom mapping. It overloads the base type
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <param name="headers">the definition mapping for the columns to class</param>
        /// <returns>Enumerable objects of the type</returns>
        public IEnumerable<T> RetrieveData<T>(Dictionary<int, string> headers) where T : class
        {
            if (headers == null) throw new ArgumentNullException(nameof(headers));

            //assign newly mapped data
            this.HeaderRow = headers;

            return this.MapData<T>();
        }
        #endregion
    }
}