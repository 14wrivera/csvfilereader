﻿namespace CsvFileReader.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception for date formats
    /// </summary>
    [Serializable]
    public class CsvDateFormatException : Exception
    {
        #region Public Properties
        public string FilePath { get; set; }
        public int Line { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// pass in the message for this constructor
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the message</param>
        public CsvDateFormatException(string message) : base(message) { }

        /// <summary>
        /// pass in the message and file path
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        public CsvDateFormatException(string message, string filePath)
            : base(message)
        {
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));
            this.FilePath = filePath;
        }

        /// <summary>
        /// pass in the message and file path
        /// </summary>  
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        /// <param name="message">is a <typeparamref name="string"/> the line number</param>
        public CsvDateFormatException(string message, string filePath, int line)
            : base(message)
        {
            if (message == null || message.Equals(String.Empty)) throw new ArgumentNullException(nameof(message));
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));
            if (line == null) throw new ArgumentNullException(nameof(line));

            this.FilePath = filePath;
            this.Line = line;
        }

        /// <summary>
        /// pass in the message and file path
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvDateFormatException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// pass in the message and file path and inner exception
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvDateFormatException(string message, string filePath, Exception inner)
            : base(message, inner)
        {
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));

            this.FilePath = filePath;
        }

        /// <summary>
        /// pass in the message and file path and inner exception
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        /// <param name="message">is a <typeparamref name="string"/> the line number</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvDateFormatException(string message, string filePath, int line, Exception inner)
            : base(message, inner)
        {
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));
            if (line == null) throw new ArgumentNullException(nameof(line));

            this.FilePath = filePath;
            this.Line = line;
        }

        /// <summary>
        /// pass in serialization info
        /// </summary>
        /// <param name="info">is a <typeparamref name="SeralizationInfo"/></param>
        /// <param name="context">is a <typeparamref name="StremaingContext"/></param>
        protected CsvDateFormatException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion
    }
}
