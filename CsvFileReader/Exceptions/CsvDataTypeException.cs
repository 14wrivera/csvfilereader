﻿namespace CsvFileReader.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception for Data type conflicts
    /// </summary>
    [Serializable]
    public class CsvDataTypeException : Exception
    {

        #region Constructor
        /// <summary>
        /// pass in the message for this constructor
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the message</param>
        public CsvDataTypeException(string message) : base(message) { }

        /// <summary>
        /// pass in the message and file path
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvDataTypeException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// pass in serialization info
        /// </summary>
        /// <param name="info">is a <typeparamref name="SeralizationInfo"/></param>
        /// <param name="context">is a <typeparamref name="StremaingContext"/></param>
        protected CsvDataTypeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion
    }
}

