﻿namespace CsvFileReader.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception for empty row
    /// </summary>
    [Serializable]
    public class CsvEmptyRowException : Exception
    {
        #region Public Properties
        public String FilePath { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// pass in the message for this constructor
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the message</param>
        public CsvEmptyRowException(string message) : base(message) { }

        /// <summary>
        /// pass in the message and file path
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        public CsvEmptyRowException(string message, string filePath)
            : base(message)
        {
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));

            this.FilePath = filePath;
        }

        /// <summary>
        /// pass in the message and file path
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvEmptyRowException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// pass in the message and file path and inner exception
        /// </summary>
        /// <param name="message">is a <typeparamref name="string"/> the error message</param>
        /// <param name="filePath">is a <typeparamref name="string"/> the file path that the error was found on</param>
        /// <param name="inner">is a <typeparamref name="Exception"/> the inner exception to add</param>
        public CsvEmptyRowException(string message, string filePath, Exception inner)
            : base(message, inner)
        {
            if (filePath == null || filePath.Equals(String.Empty)) throw new ArgumentNullException(nameof(filePath));

            this.FilePath = filePath;
        }

        /// <summary>
        /// pass in serialization info
        /// </summary>
        /// <param name="info">is a <typeparamref name="SeralizationInfo"/></param>
        /// <param name="context">is a <typeparamref name="StremaingContext"/></param>
        protected CsvEmptyRowException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion
    }
}
