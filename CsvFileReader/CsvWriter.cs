﻿namespace CsvFileReader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using CsvFileReader.Decorators;
    using CsvFileReader.Interfaces;
    using CsvFileReader.Models;

    using Interfaces.Models;

    public class CsvWriter : ICsvWriter
    {
        public IFileWriter FileWriter { get; set; }
        public char Delimiter { get; private set; }

        #region Constructor
        /// <summary>
        /// instantiate the object with a default delimiter
        /// </summary>
        /// <param name="fileWrite">is a<typeparamref name="IFileWrite"/>. the delimiter</param>
        public CsvWriter(IFileWriter fileWriter, char delimiter)
        {
            if (fileWriter == null) throw new ArgumentNullException(nameof(fileWriter));
            this.FileWriter = fileWriter;
            this.Delimiter = delimiter;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <param name="data">is a<typeparamref name="IEnumerable<T> data"/>. The data to write</param>
        /// <param name="filePath">is a<typeparamref name="String"/>. The file to write to</param>
        public void Write<T>(IEnumerable<T> data, string filePath) where T : class
        {
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (filePath == null ||filePath.Equals(string.Empty)) throw new ArgumentNullException(nameof(filePath));

            var fileData = new StringBuilder();
            //Generate Headers
            var headers = this.GenerateHeaders(data);
            fileData.Append($"{string.Join(this.Delimiter.ToString(), headers.Select(m => m.PropertyName))}\r\n");

            //Generate data
            var rows = this.GenerateData(headers, data);
            var body = string.Join("\r\n", rows);
            fileData.Append(body);
            //Write
            this.FileWriter.WriteFile(fileData, filePath);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="data">The data to get</param>
        /// <returns>collection of file lines</returns>
        private List<TypeMetaData> GenerateHeaders<T>(IEnumerable<T> data) where T : class
        {
            var headers = new List<TypeMetaData>();
            int i = 0;
            //get the names from the properties and attributes
            foreach (var info in typeof(T).GetProperties())
            {
                i++;
                //retrieve property name or if CsvMapTo decorator is used then CsvMapTo name
                var attribute = info.GetCustomAttributes(typeof(CsvMapTo), false).Cast<CsvMapTo>().SingleOrDefault();
                var ignore = info.GetCustomAttributes(typeof(CsvIgnoreProperty), false).Cast<CsvIgnoreProperty>().SingleOrDefault();
                var list = info.GetCustomAttributes(typeof(CsvListProperty), false).Cast<CsvListProperty>().SingleOrDefault();

                if (ignore != null) continue;

                if (attribute != null)
                {
                    headers.Add(new TypeMetaData(info, i, attribute.Name));
                }
                else if (!string.IsNullOrEmpty(list?.Name))
                {
                    headers.Add(new TypeMetaData(info, i, list.Name));
                }
                else
                {
                    headers.Add(new TypeMetaData(info, i, info.Name));
                }
            }
            return headers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="headers">The MetaData for header</param>
        /// <param name="data">The data to get</param>
        /// <returns>collection of file lines</returns>
        private IEnumerable<string> GenerateData<T>(List<TypeMetaData> headers, IEnumerable<T> data) where T : class
        {
            var dataRow = new List<string>();
            //get the names from the properties and attributes
            foreach (var item in data)
            {
                var row = new StringBuilder();
                foreach (var headerItem in headers.OrderBy(m => m.ColumnIndex))
                {
                    row.Append($"{headerItem.RetrieveValue(item)}{this.Delimiter}");
                }
                if (row.Length <= 1)
                {
                    continue;
                }
                row = row.Remove(row.Length - 1, 1);
                dataRow.Add(row.ToString());
            }
            return dataRow;
        }
        #endregion
    }
}
