﻿namespace CsvFileReader.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for CSV writing
    /// </summary>
    public interface ICsvWriter
    {
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <param name="data">is a<typeparamref name="IEnumerable<T> data"/>. The data to write</param>
        /// <param name="filePath">is a<typeparamref name="String"/>. The file to write to</param>
        void Write<T>(IEnumerable<T> data, string filePath) where T: class;
    }
}
