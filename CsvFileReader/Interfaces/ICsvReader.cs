﻿namespace CsvFileReader.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface for CSV reading
    /// </summary>
    public interface ICsvReader
    {
        Boolean HasRows { get; }

        /// <summary>
        /// return Enumerable objects of a type for each record in the csv.
        /// first it pulls out the headers from the first row of the csv and maps them to the class
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <returns>Enumerable objects of the type</returns>
        IEnumerable<T> RetrieveData<T>() where T : class;

        /// <summary>
        /// this method allows for custom mapping to csv files and removes header row
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <param name="headers">the definition mapping for the columns to class</param>
        /// <returns>Enumerable objects of the type</returns>
        IEnumerable<T> RetrieveDataRemoveFirstRow<T>(Dictionary<int, string> headers) where T : class;


        /// <summary>
        /// this method allows for custom mapping. It overloads the base type
        /// </summary>
        /// <typeparam name="T">The type to cast to</typeparam>
        /// <param name="headers">the definition mapping for the columns to class</param>
        /// <returns>Enumerable objects of the type</returns>
        IEnumerable<T> RetrieveData<T>(Dictionary<int, string> headers) where T : class;
    }
}
