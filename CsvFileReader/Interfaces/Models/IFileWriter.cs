﻿namespace CsvFileReader.Interfaces.Models
{
    using System.Text;

    /// <summary>
    /// this interface is for parsing files
    /// </summary>
    public interface IFileWriter
    {
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <param name="data">is a<typeparamref name="StringBuilder"/>. The data to write</param>
        /// <param name="filePath">is a<typeparamref name="String"/>. The file to write to</param>
        void WriteFile(StringBuilder data, string filePath);
    }
}
