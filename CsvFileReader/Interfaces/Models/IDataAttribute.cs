﻿namespace CsvFileReader.Interfaces.Models
{
    using System;

    /// <summary>
    /// data attributes of supported types
    /// </summary>
    public interface IDataAttribute
    {
        /// <summary>
        /// Set a property type for an object
        /// </summary>
        /// <param name="obj">The model to bind to</param>
        /// <param name="column">is a <typeparamref name="String"/>. The column name of the CSV file also the property name of the model</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The value</param>
        void Resolve(Object obj, String column, String contents);
    }
}
