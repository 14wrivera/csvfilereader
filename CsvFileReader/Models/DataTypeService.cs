﻿namespace CsvFileReader.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    
    using CsvFileReader.Exceptions;
    using CsvFileReader.Interfaces.Models;
    using CsvFileReader.Models.Types;

    /// <summary>
    /// this class resolves the types of the models and the string content from the csv file
    /// </summary>
    public class DataTypeService : IDataAttribute
    {
        #region Private Methods
        /// <summary>
        /// This method is used to resolve generic types
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object infoThe column or property name</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        private void ResolveGeneric(Object obj, PropertyInfo info, String contents)
        {
            //Nullable int support only for now
            if (info.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                //get type in collection and check if its an int
                Type itemType = info.PropertyType.GetGenericArguments()[0];
                if (itemType.Name.Equals("Int32"))
                {
                    IntAttributeService.Retrieve().ResolveNullableInt(obj, info, contents);
                }
            }
            //list support
            else if (info.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
            {
                ListAttributeService.Retrieve().ResolveList(obj, info, contents);
            }
        }

        /// <summary>
        /// This method determines if the generic type is currently supported
        /// </summary>
        /// <param name="name">is a <paramref name="Type"/>the type</param>
        /// <returns>true or false</returns>
        private bool GenericSupported(Type name)
        {
            bool supported = false;
            if (name == typeof(Nullable<>))
            {
                supported = true;
            }
            else if (name == typeof(List<>))
            {
                supported = true;
            }
            return supported;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// This method resolves the properties to the type given
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="column">is a <typeparamref name="String"/>. The column or property name</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void Resolve(Object obj, String column, String contents)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            if (column == null || column.Equals(String.Empty)) throw new ArgumentNullException(nameof(column));
           
            try
            {
                //get the property info
                PropertyInfo info = obj.GetType().GetProperty(column);
                string name = info.PropertyType.Name;

                if (info.PropertyType.IsGenericType &&
                    this.GenericSupported(info.PropertyType.GetGenericTypeDefinition()))
                {
                    this.ResolveGeneric(obj, info, contents);
                }
                else if (name.Equals("Int32"))
                {
                    IntAttributeService.Retrieve().ResolveInt(obj, info, contents);
                }
                else if (name.Equals("DateTime"))
                {
                    DateAttributeService.Retrieve().ResolveDate(obj, info, contents);
                }
                else if (name.Equals("String"))
                {
                    info.SetValue(obj, contents, null);
                }
                else if (info.PropertyType.IsClass)
                {
                    ObjectAttributeService.Retrieve().ResolveObject(obj, info, contents);
                }
                else
                {
                    throw new CsvDataTypeException("Property Type: " + name + " is not supported");
                }
            }
            catch (NullReferenceException exc)
            {
                throw new CsvPropertyDecoratorException(column + "is not a property of " + obj, exc);
            }
        }
        #endregion
    }
}
