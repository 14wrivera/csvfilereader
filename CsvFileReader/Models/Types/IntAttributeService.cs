﻿namespace CsvFileReader.Models.Types
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Int type service resolves int type values
    /// </summary>
    public class IntAttributeService
    {
        #region Private Static Members
        private static IntAttributeService instance;
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Singleton Instance
        /// </summary>
        /// <returns></returns>
        public static IntAttributeService Retrieve()
        {
            return IntAttributeService.instance;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Singleton Constructor
        /// </summary>
        static IntAttributeService()
        {
            IntAttributeService.instance = new IntAttributeService();
        }
        /// <summary>
        /// Singleton class so a private constructor is needed
        /// </summary>
        private IntAttributeService() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// resolve int types
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveInt(Object obj, PropertyInfo info, String contents)
        {
            if (String.IsNullOrWhiteSpace(contents))
            {
                //set default to 0
                info.SetValue(obj, 0, null);
            }
            else
            {
                info.SetValue(obj, int.Parse(contents), null);
            }
        }
        /// <summary>
        /// resolve nullable ints
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveNullableInt(Object obj, PropertyInfo info, string contents)
        {
            if (String.IsNullOrWhiteSpace(contents))
            {
                //set default to 0
                info.SetValue(obj, null, null);
            }
            else
            {
                info.SetValue(obj, int.Parse(contents), null);
            }
        }
        #endregion
    }
}
