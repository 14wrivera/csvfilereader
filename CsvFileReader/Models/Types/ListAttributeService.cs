﻿namespace CsvFileReader.Models.Types
{
    using System;
    using System.Linq;
    using System.Reflection;

    using CsvFileReader.Decorators;
    using CsvFileReader.Exceptions;

    /// <summary>
    /// resolve List type values
    /// </summary>
    public class ListAttributeService
    {
        #region Private Static Members
        private static ListAttributeService instance;
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Singleton Instance
        /// </summary>
        /// <returns></returns>
        public static ListAttributeService Retrieve()
        {
            return ListAttributeService.instance;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Singleton Constructor
        /// </summary>
        static ListAttributeService()
        {
            ListAttributeService.instance = new ListAttributeService();
        }
        /// <summary>
        /// Singleton class so a private constructor is needed
        /// </summary>
        private ListAttributeService() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// resolve lists
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveList(Object obj, PropertyInfo info, string contents)
        {
            CsvListProperty attribute = info.GetCustomAttributes(typeof(CsvListProperty), false).Cast<CsvListProperty>().SingleOrDefault();
            if (attribute != null)
            {
                if (attribute.Delimiter == '\0')
                    throw new CsvPropertyDecoratorException(info.Name + ": Missing Delimiter. Lists need a defined Delimiter");

                //get type in list
                Type itemType = info.PropertyType.GetGenericArguments()[0];
                //instantiate the list 
                Object list = Activator.CreateInstance(info.PropertyType.Assembly.FullName, info.PropertyType.FullName).Unwrap();
                //split contents by defined delimiter
                foreach (string item in contents.Split(attribute.Delimiter))
                {
                    SetValueByType(info, attribute, itemType, list, item);
                }
                info.SetValue(obj, list, null);
            }
            else
            {
                throw new CsvPropertyDecoratorException(info.Name + ": Missing Attribute Decorator of CsvPropertyAttribute. Lists need a defined Delimiter");
            }
        }

        /// <summary>
        /// Sets the vale based on the type
        /// </summary>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="attribute">is a <typeparamref name="CsvListProperty"/>. Attribute this property has</param>
        /// <param name="itemType">is a <typeparamref name="Type"/>. the type</param>
        /// <param name="list">the list to bind to</param>
        /// <param name="item">the value</param>
        private void SetValueByType(PropertyInfo info, CsvListProperty attribute, Type itemType, Object list, string item)
        {
            if (itemType.IsClass)
            {
                ObjectAttributeService.Retrieve().ResolveObjectList(itemType, attribute, list, info, item);
            }
            else if (itemType.Name.Equals("Int32"))
            {
                list.GetType().GetMethod("Add").Invoke(list, new object[] { int.Parse(item) });
            }
            else if (itemType.Name.Equals("String"))
            {
                list.GetType().GetMethod("Add").Invoke(list, new object[] { item });
            }
            else if (itemType.Name.Equals("DateTime"))
            {
                DateAttributeService.Retrieve().ResolveListDate(list, info, item);
            }
        }
        #endregion
    }
}
