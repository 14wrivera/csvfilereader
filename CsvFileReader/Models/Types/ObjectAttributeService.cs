namespace CsvFileReader.Models.Types
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;

    using CsvFileReader.Decorators;
    using CsvFileReader.Exceptions;

    /// <summary>
    /// resolve object type values
    /// </summary>
    public class ObjectAttributeService
    {
        #region Private Static Members
        private static ObjectAttributeService instance;
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Singleton Instance
        /// </summary>
        /// <returns></returns>
        public static ObjectAttributeService Retrieve()
        {
            return ObjectAttributeService.instance;
        }
        #endregion

        #region Private Members
        private DataTypeService attributeService;
        #endregion

        #region Constructor
        /// <summary>
        /// Thread safe private constructor
        /// </summary>
        static ObjectAttributeService()
        {
            ObjectAttributeService.instance = new ObjectAttributeService();
        }
        /// <summary>
        /// Singleton class so a private constructor is needed
        /// </summary>
        private ObjectAttributeService()
        {
            attributeService = new DataTypeService();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// resolve Custom types
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveObject(Object obj, PropertyInfo info, String contents)
        {
            CsvListProperty attribute = info.GetCustomAttributes(typeof(CsvListProperty), false).Cast<CsvListProperty>().SingleOrDefault();
            if (attribute != null)
            {
                var nobj = Activator.CreateInstance(info.PropertyType.Assembly.FullName, info.PropertyType.FullName).Unwrap();
                attributeService.Resolve(nobj, attribute.Name, contents);
                info.SetValue(obj, nobj, null);
            }
            else
            {
                throw new CsvPropertyDecoratorException(info.Name + ": Missing Attribute Decorator of CsvPropertyAttribute");
            }
        }
        /// <summary>
        /// helps resolve objects in a list
        /// </summary>
        /// <param name="itemType">is a <typeparamref name="Type"/>. The type of items</param>
        /// <param name="attribute">is a <typeparamref name="CsvListProperty"/>. CsvListProperty Decorator attribute</param>
        /// <param name="list">the list to add items to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveObjectList(Type itemType, CsvListProperty attribute, Object list, PropertyInfo info, string contents)
        {
            //add item to collection
            Object type = Activator.CreateInstance(itemType);
            list.GetType().GetMethod("Add").Invoke(list, new object[] { type });

            //bind current attributes 
            TypeDescriptor.AddAttributes(type, info.GetCustomAttributes(typeof(CsvListProperty), false) as Attribute[]);

            attributeService.Resolve(type, attribute.Name, contents);
        }
        #endregion
    }
}
