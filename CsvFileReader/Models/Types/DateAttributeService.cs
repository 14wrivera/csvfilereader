﻿namespace CsvFileReader.Models.Types
{
    using System;
    using System.Linq;
    using System.Reflection;

    using CsvFileReader.Decorators;
    using CsvFileReader.Exceptions;

    /// <summary>
    /// Date Service Resolves Date type values
    /// </summary>
    public class DateAttributeService
    {
        #region Private Static Members
        private static DateAttributeService instance;
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Singleton Instance
        /// </summary>
        /// <returns></returns>
        public static DateAttributeService Retrieve()
        {
            return DateAttributeService.instance;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Thread safe private constructor
        /// </summary>
        static DateAttributeService()
        {
            DateAttributeService.instance = new DateAttributeService();
        }

        /// <summary>
        /// Singleton class so a private constructor is needed
        /// </summary>
        private DateAttributeService() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// resolve Date
        /// </summary>
        /// <param name="obj">This is the instantiated model that we are mapping properties to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveDate(Object obj, PropertyInfo info, String contents)
        {
            try
            {
                //attempt to use the custom attribute if present
                CsvDate attribute = info.GetCustomAttributes(typeof(CsvDate), false).Cast<CsvDate>().SingleOrDefault();
                info.SetValue(obj, this.ResolveDate(attribute, contents), null);
            }
            catch (FormatException exc)
            {
                throw new CsvDateFormatException(exc.Message + " on Object Data:" + info.Name);
            }
        }

        /// <summary>
        /// resolve Date based on attribute
        /// </summary>
        /// <param name="attribute">is a <typeparamref name="CsvDate"/>. This is the date attribute defined by the model</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public DateTime ResolveDate(CsvDate attribute, string contents)
        {
            if (attribute != null)
            {
                return attribute.Resolve(contents);
            }
            else
            {
                //if not custom then just use good old convert to date time
                return Convert.ToDateTime(contents);
            }

        }

        /// <summary>
        /// Helps bind date time values to a list
        /// </summary>
        /// <param name="list">The list we are adding the date object to</param>
        /// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
        /// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
        public void ResolveListDate(Object list, PropertyInfo info, String contents)
        {
            //bind current attribute add atts
            CsvDate dateAttribute = info.GetCustomAttributes(typeof(CsvDate), false).Cast<CsvDate>().SingleOrDefault();
            try
            {
                DateTime type = this.ResolveDate(dateAttribute, contents);
                list.GetType().GetMethod("Add").Invoke(list, new object[] { type });
            }
            catch (FormatException exc)
            {
                throw new CsvDateFormatException(exc.Message + " on list property:" + info.Name);
            }
        }
        #endregion
    }
}
