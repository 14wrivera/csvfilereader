namespace CsvFileReader.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CsvFileReader.Interfaces.Models;

    /// <summary>
    /// class parses csv files
    /// </summary>
    public class FileParser : IFileParser
    {
        #region Public Properties
        public char Delimiter { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// instantiate the object with a default delimiter
        /// </summary>
        /// <param name="delimiter">is a<typeparamref name="char"/>. the delimiter</param>
        public FileParser(char delimiter)
        {
            if (delimiter == null) throw new ArgumentNullException(nameof(delimiter));
            this.Delimiter = delimiter;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Parses csv rows with a supplied delimiter. This will ignore commas in between double quotes
        /// </summary>
        /// <param name="str">The string to search through</param>
        /// <returns>return cells in a collection</returns>
        public List<string> ParseRow(string str)
        {
            if (str == null) throw new ArgumentNullException(nameof(str));

            int startIndex = 0;
            int QuoteCont = 0;
            List<String> content = new List<String>();
            Char[] chrs = str.ToCharArray();
            //loop through all characters in the string
            for (int i = 0; i < chrs.Count(); i++)
            {
                //if there is a quote present and no backslash preceding then increment quote count
                if (chrs[i] == '"')
                {
                    if (i - 1 < 0)
                        QuoteCont++;
                    else if (chrs[i - 1] != '\\')
                        QuoteCont++;
                }
                //if character is the delimiter and quote count is even the split off contents
                if (chrs[i] == this.Delimiter && QuoteCont % 2 == 0)
                {
                    int end = i - startIndex;
                    //add content and remove escaped quotes
                    content.Add(str.Substring(startIndex, end).Replace("\"", "").Trim());
                    startIndex = i + 1;

                    //if the last character is the delimiter before a newline then just return empty string
                    if (chrs[i] == this.Delimiter && i + 1 == str.Length)
                        content.Add(string.Empty);
                }
                //if this is the end of the string then just store remaining content as even quotes does not matter at this point.
                else if (i + 1 == str.Length)
                {
                    int end = (i + 1) - startIndex;
                    //add content and remove escaped quotes
                    content.Add(str.Substring(startIndex, end).Replace("\"", "").Trim());
                }
            }
            return content;
        }
        #endregion
    }
}
