﻿namespace CsvFileReader.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using CsvFileReader.Decorators;

    /// <summary>
    /// Type information for a given class
    /// </summary>
    public class TypeMetaData
    {
        public PropertyInfo Property { get; private set; }

        public string PropertyName { get; private set; }

        public int ColumnIndex { get; private set; }

        private bool isCsvList;

        private bool isCsvListPrimitive;

        private char delimiter;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyInfo">the property to map</param>
        /// <param name="columnIndex">column index on file</param>
        /// <param name="name">the name of the column</param>
        public TypeMetaData(PropertyInfo propertyInfo, int columnIndex, string name)
        {
            if (propertyInfo == null) throw new ArgumentNullException(nameof(propertyInfo));
            if (name == null || name.Equals(string.Empty)) throw new ArgumentNullException(nameof(name));
            if (columnIndex < 1) throw new ArgumentException($"{nameof(columnIndex)} : must be Greater than 0");

            this.Property = propertyInfo;
            this.PropertyName = name;
            this.ColumnIndex = columnIndex;
            this.isCsvList = this.Property.GetCustomAttributes(typeof(CsvListProperty), false).Any();
            if (this.isCsvList)
            {
                var attribute = this.Property.GetCustomAttributes(typeof(CsvListProperty), false).Cast<CsvListProperty>().FirstOrDefault();
                this.isCsvListPrimitive = attribute.Name == null;
                this.delimiter = attribute.Delimiter;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="item">the item to get the value of</param>
        /// <returns>String column contents</returns>
        public string RetrieveValue<T>(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            if (this.isCsvList)
            {
                var properties = this.Property.GetValue(item, null) as IList;
                var itemTypeInfo = properties?.GetType().GetGenericArguments()[0].GetProperty(this.PropertyName);
                
                if (this.isCsvListPrimitive)
                {
                    var items = properties?.Cast<object>().Select(m => m.ToString());
                    if (items != null)
                        return $"\"{string.Join(this.delimiter.ToString(), items)}\"";
                }
                else
                {
                    var items = properties?.Cast<object>().Select(m => itemTypeInfo.GetValue(m, null).ToString());
                    if (items != null)
                        return $"\"{string.Join(this.delimiter.ToString(), items)}\"";
                }
            }
            else
            {
                return this.Property.GetValue(item, null) as string;
            }
            return string.Empty;
        }
    }
}
