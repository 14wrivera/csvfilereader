﻿namespace CsvFileReader.Models
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Text;

    using CsvFileReader.Interfaces.Models;

    /// <summary>
    /// Write to File
    /// </summary>
    public class FileWriter:IFileWriter
    {
        #region Public Properties
        public FileInfo FileInfo { get; private set; }
        #endregion

        /// <summary>
        /// Checks where Directory has write permissions
        /// </summary>
        /// <returns>True/False</returns>
        #region Private Methods
        private bool HasWritePermissions()
        {
            var accessRules = this.FileInfo.Directory.GetAccessControl().GetAccessRules(true, true,
                                typeof(System.Security.Principal.SecurityIdentifier));
            if (accessRules == null)
                return false;

            return !accessRules.Cast<FileSystemAccessRule>().
                Where(rule => (FileSystemRights.Write & rule.FileSystemRights) == FileSystemRights.Write).
                Any(rule => rule.AccessControlType == AccessControlType.Deny);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <param name="data">is a<typeparamref name="StringBuilder"/>. The data to write</param>
        /// <param name="filePath">is a<typeparamref name="String"/>. The file to write to</param>
        public void WriteFile(StringBuilder data, string filePath) 
        {
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath));
            this.FileInfo = new FileInfo(filePath);
            //check if directory exists
            if (!this.FileInfo.Directory.Exists)
                throw new DirectoryNotFoundException(nameof(filePath));
            //check if permission allows write
            if (!this.HasWritePermissions())
                throw new AccessViolationException(nameof(this.FileInfo.DirectoryName));

            File.WriteAllText(this.FileInfo.FullName, data.ToString());
        }
        #endregion
    }
}
