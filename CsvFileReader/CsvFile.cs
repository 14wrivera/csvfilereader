﻿namespace CsvFileReader
{
    using System;

    using CsvFileReader.Interfaces;
    using CsvFileReader.Interfaces.Models;
    using CsvFileReader.Models;

    /// <summary>
    /// This is a container for using CsvReaders
    /// </summary>
    public class CsvFile
    {
        #region Public Properties
        public ICsvReader Reader { get; private set; }
        public ICsvWriter Writer { get; private set; }
        public IFileParser Parser { get; private set; }
        public IFileWriter FileWriter { get; private set; }
        public IDataAttribute Mapper { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// base constructor with a csv parser defined
        /// </summary>
        /// <param name="path">is a <typeparamref name="string"/> The path of the file</param>
        public CsvFile(string path)
        {
            if(string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            this.Parser = new FileParser(',');
            this.FileWriter = new FileWriter();
            this.Mapper = new DataTypeService();
            this.Reader = new CsvReader(path, this.Parser, this.Mapper);
            this.Writer = new CsvWriter(this.FileWriter,',');
        }
        #endregion
    }
}
