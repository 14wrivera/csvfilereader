﻿using CsvFileReader;
using CsvFileReader.Decorators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using CsvFileReaderTest.Model;

namespace CsvFileReaderTest
{
    /// <summary>
    ///This is a test class for CsvDateAttributeTest and is intended
    ///to contain all CsvDateAttributeTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CsvDateDecoratorTest
    {
        /// <summary>
        ///A test for CsvDateAttribute Constructor
        ///</summary>
        [TestMethod()]
        public void CsvDateAttributeConstructorTestWithTry()
        {
            string format = "MMddyyyy";
            bool Try = true;
            CsvDate target = new CsvDate(format, Try);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for CsvDateAttribute Constructor
        ///</summary>
        [TestMethod()]
        public void CsvDateAttributeConstructorTest()
        {
            string format = "MMddyyyy";
            CsvDate target = new CsvDate(format);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for Resolve
        ///</summary>
        [TestMethod()]
        public void ResolveTest()
        {
            string format = "yyyyMMdd";
            CsvDate target = new CsvDate(format);
            string content = "20120304";
            DateTime expected = new DateTime(2012, 03, 04);
            DateTime actual;
            actual = target.Resolve(content);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Resolve with different formatted date
        ///</summary>
        [TestMethod()]
        public void ResolveTestDiff()
        {
            string format = "yyyy/M/d";
            CsvDate target = new CsvDate(format);
            string content = "2012/3/4";
            DateTime expected = new DateTime(2012, 03, 04);
            DateTime actual;
            actual = target.Resolve(content);
            Assert.AreEqual(expected, actual);
        }
    }
}
