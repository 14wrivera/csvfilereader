﻿using CsvFileReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CsvFileReader.Exceptions;
using CsvFileReader.Interfaces.Models;
using CsvFileReader.Models;
using System.Collections.Generic;
using System.IO;

using CsvFileReaderTest.Model;

namespace CsvFileReaderTest
{
    /// <summary>
    /// Set of test for CsvAttributeTest
    /// </summary>
    [TestClass]
    public class CsvAttributeTest
    {
        /// <summary>
        /// initialize test data paths classes etc..
        /// </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            this.dataPath = "../../../Tests/TestData/";
            this.fileParser = new FileParser(',');
            this.Mapper = new DataTypeService();
        }
        string dataPath;
        IFileParser fileParser;
        IDataAttribute Mapper;
        /// <summary>
        /// this test that when using an object for property that the decorator is explicitly used
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(CsvAttributeException))]
        public void ThrowException()
        {
            CsvReader target = new CsvReader(this.dataPath + "Comment.csv", this.fileParser, this.Mapper);
            target.RetrieveData<CustomExceptionThrownModel>();
        }
        /// <summary>
        /// this test ensures that we can instantiate objects for properties and map the data to the right property 
        /// on the new model. Decorators are used
        /// </summary>
        [TestMethod]
        public void customAttributePropagation()
        {
            CustomObject type = new CustomObject();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "SID", "12345");
            Assert.AreEqual(type.SID.SID, 12345);
        }
        /// <summary>
        /// this test ensures that we can instantiate objects for properties and map the data to the right property 
        /// on the new model. Decorators are used. This also allows for multiples or Enumerable types to be used
        /// </summary>
        [TestMethod]
        public void customAttributePropagationWList()
        {
            listPropertyModel type = new listPropertyModel();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "SID", "12345,456");
            Assert.AreEqual(type.SID[0].SID, 12345);
            Assert.AreEqual(type.SID[1].SID, 456);
        }

        /// <summary>
        /// this test ensures that we can instantiate objects for properties and map the data to the right property. 
        /// Decorators are used. This also allows for multiples or Enumerable types to be used
        /// </summary>
        [TestMethod]
        public void customAttributePropagationWListPrimitive()
        {
            listGenericModel type = new listGenericModel();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "OrgID", "1,3");
            Assert.AreEqual(type.OrgID[0], 1);
            Assert.AreEqual(type.OrgID[1], 3);
        }

        /// <summary>
        /// this test ensures that nullables are accepted when the type is used in the model
        /// </summary>
        [TestMethod]
        public void nullablePropertyTypeTest()
        {
            NullablePropertyModel type = new NullablePropertyModel();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "OrgID", "");
            Assert.AreEqual(type.OrgID, null);
        }

        /// <summary>
        /// this test ensures that we can instantiate objects for properties and map the data to the right property. 
        /// Decorators are used. This also allows for multiples or Enumerable types to be used
        /// </summary>
        [TestMethod]
        public void customAttributePropagationWListDate()
        {
            listGenericModel type = new listGenericModel();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "SID", "12102012,12112012");
            Assert.AreEqual(type.SID[0].Month, 12);
            Assert.AreEqual(type.SID[1].Day, 11);
        }


        /// <summary>
        /// this test for recursive objects such as a model that contains property of the same type
        /// </summary>
        [TestMethod]
        public void customAttributePropagationRecursive()
        {
            RecursivePropergationModel type = new RecursivePropergationModel();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "SID", "12345");
            Assert.AreEqual(type.SID.SID, null);
        }

        /// <summary>
        ///A test for validating the type is not available
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(CsvDataTypeException))]
        public void CsvDataTypeExceptionTest()
        {
            //unsupported type uses testModel for a property type
            unsupportedType type = new unsupportedType();
            DataTypeService service = new DataTypeService();
            service.Resolve(type, "SID", "12345");
        }
    }
}
