﻿using CsvFileReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CsvFileReader.Exceptions;
using CsvFileReader.Interfaces.Models;
using CsvFileReader.Models;
using System.Collections.Generic;
using System.IO;

using CsvFileReaderTest.Model;

namespace CsvFileReaderTest
{


    /// <summary>
    ///This is a test class for CsvReaderTest and is intended
    ///to contain all CsvReaderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CsvReaderTest
    {
        #region Additional test attributes
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            this.dataPath = "../../../Tests/TestData/";
            this.fileParser = new FileParser(',');
            this.Mapper = new DataTypeService();
        }
        string dataPath;
        IFileParser fileParser;
        IDataAttribute Mapper;

        #endregion


        /// <summary>
        ///A test for CsvReader to test empty file exception
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(CsvEmptyFileException))]
        public void RetrieveDataFromConstructorExceptionTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "EmptyFile.csv", this.fileParser, this.Mapper);
        }
        /// <summary>
        ///A test to retrieve Data and verify the collection count
        ///</summary>
        [TestMethod()]
        public void RetrieveDataToModel()
        {
            CsvReader target = new CsvReader(this.dataPath + "Comment.csv", this.fileParser, this.Mapper);
            List<Comment> data = target.RetrieveData<Comment>() as List<Comment>;
            Assert.AreEqual(1, data.Count);
        }

        /// <summary>
        ///A test to retrieve Data and verify that instance properties are set on the csv reader
        ///</summary>
        [TestMethod()]
        public void RetrieveDataTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "Comment.csv", this.fileParser, this.Mapper);
            Assert.AreEqual(true, target.HasRows);
            Assert.AreEqual(2, target.DataRows.Count);
        }

        /// <summary>
        ///A test to retrieve Data with extra columns that are not properties on model
        ///</summary>
        [TestMethod()]
        public void RetrieveDataExtraColTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "ExtraColComment.csv", this.fileParser, this.Mapper);
            Assert.AreEqual(true, target.HasRows);
            Assert.AreEqual(2, target.DataRows.Count);
        }

        /// <summary>
        ///A test for CsvPropertyNotFoundException
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(CsvPropertyNotFoundException))]
        public void CsvPropertyNotFoundExceptionTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "BadHeaderComment.csv", this.fileParser, this.Mapper);
            target.RetrieveData<Comment>();
        }

        // <summary>
        ///A test for CsvEmptyRowException exception 
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(CsvEmptyRowException))]
        public void CsvEmptyRowExceptionTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "NoComment.csv", this.fileParser, this.Mapper);
            target.RetrieveData<Comment>();
        }
        /// <summary>
        /// this method tests the reader with user defined properties on csv columns
        /// </summary>
        [TestMethod()]
        public void CsvCustomMappingTest()
        {
            Dictionary<int, string> mappings = new Dictionary<int, string>();
            mappings.Add(0, "propA"); //col maps to comment.OrgID in csv file
            mappings.Add(1, "propB");//col maps to comment.Phone in csv file
            mappings.Add(2, "propC");//col maps to comment.AcctNum in csv file
            CsvReader target = new CsvReader(this.dataPath + "Comment.csv", this.fileParser, this.Mapper);
            List<testModel> Data = target.RetrieveDataRemoveFirstRow<testModel>(mappings) as List<testModel>;
            Assert.AreEqual(375, Data[0].propA);
            Assert.AreEqual("3202543554", Data[0].propB);
            Assert.AreEqual("149301787", Data[0].propC);
        }
        /// <summary>
        /// this method tests the reader with user defined properties on csv columns
        /// and with no header row in the csv file
        /// </summary>
        [TestMethod()]
        public void CsvCustomMappingTestNoHeader()
        {
            Dictionary<int, string> mappings = new Dictionary<int, string>();
            mappings.Add(0, "propA"); //col maps to comment.OrgID in csv file
            mappings.Add(1, "propB");//col maps to comment.Phone in csv file
            mappings.Add(2, "propC");//col maps to comment.AcctNum in csv file
            CsvReader target = new CsvReader(this.dataPath + "CommentNoHeader.csv", this.fileParser, this.Mapper);
            List<testModel> Data = target.RetrieveData<testModel>(mappings) as List<testModel>;
            Assert.AreEqual(375, Data[0].propA);
            Assert.AreEqual("3202543554", Data[0].propB);
            Assert.AreEqual("149301787", Data[0].propC);
        }
        /// <summary>
        /// this method tests the reader with user defined property mapping attribute
        /// </summary>
        [TestMethod()]
        public void CsvAttributeMappingTest()
        {
            CsvReader target = new CsvReader(this.dataPath + "Comment.csv", this.fileParser, this.Mapper);
            List<AttributeTestModel> Data = target.RetrieveData<AttributeTestModel>() as List<AttributeTestModel>;
            Assert.AreEqual(375, Data[0].propA);
            Assert.AreEqual("3202543554", Data[0].propB);
            Assert.AreEqual("149301787", Data[0].propC);
            Assert.AreEqual(new DateTime(2012, 9, 19), Data[0].D);
        }
    }
}
