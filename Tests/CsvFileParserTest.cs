﻿using CsvFileReader.Models;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using CsvFileReaderTest.Model;

namespace CsvFileReaderTest
{
    using System.Linq;

    /// <summary>
    ///This is a test class for CsvFileParserTest and is intended
    ///to contain all CsvFileParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CsvFileParserTest
    {
        /// <summary>
        ///A test for FileParser ParseRow
        ///</summary>
        [TestMethod()]
        public void ParseRowEmptyFirstRowTest()
        {
            FileParser target = new FileParser(',');
            string row = ",\"tes,t\",more";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.AreEqual(rows[0], "");
        }

        /// <summary>
        ///A test for FileParser ParseRow with extra comma
        ///</summary>
        [TestMethod()]
        public void ParseRowEmptyLastRowTest()
        {
            FileParser target = new FileParser(',');
            string row = "\"tes,t\",more,";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.AreEqual(rows[2], "");
        }

        /// <summary>
        ///A test for ParseRow when no quotes added
        ///</summary>
        [TestMethod]
        public void ParseRowNoQuotesRowTest()
        {
            FileParser target = new FileParser(',');
            string row = "tes,test3,more";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.IsFalse(rows.Any(m => m == String.Empty));
        }

        /// <summary>
        ///A test for ParseRow when quotes added
        ///</summary>
        [TestMethod]
        public void ParseRowQuotesRowTest()
        {
            FileParser target = new FileParser(',');
            string row = "tes,\"test3,ttt,more\",more";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.IsFalse(rows.Any(m => m == String.Empty));
            Assert.AreEqual("test3,ttt,more", rows[1]);
        }

        /// <summary>
        /// Test that multiple quotes get removed properly
        /// </summary>
        [TestMethod]
        public void ParseRowWithManyQuotesRowTest()
        {
            FileParser target = new FileParser(',');
            string row = "tes,\"\"\"test3,ttt,more\"\"\",more";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.IsFalse(rows.Any(m => m == String.Empty));
            Assert.AreEqual("test3,ttt,more", rows[1]);

        }
    }
}
