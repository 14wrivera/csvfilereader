﻿namespace Tests
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using CsvFileReader;
    using CsvFileReader.Decorators;
    using CsvFileReader.Interfaces.Models;
    using CsvFileReader.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    public class CsvFileTest
    {
        [TestClass]
        public class Constructor
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void PathIsNullExceptionExpected()
            {
                var test = new CsvFile(null);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void PathIsEmptyExceptionExpected()
            {
                var test = new CsvFile("");
            }
        }
    }
}
