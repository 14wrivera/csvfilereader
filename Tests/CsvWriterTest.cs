﻿namespace Tests
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using CsvFileReader;
    using CsvFileReader.Decorators;
    using CsvFileReader.Interfaces.Models;
    using CsvFileReader.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    public class CsvWriterTest
    {
        public class TestClass
        {
            public string Test { get; set; }

            [CsvListProperty("Test")]
            public List<TestClass> TestList { get; set; }
        }

        [TestClass]
        public class Constructor
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void FileWriterIsNullExceptionExpected()
            {
                var test = new CsvWriter(null, '.');
            }
        }

        [TestClass]
        public class Write
        {
            private List<CsvTestClass> ObjectUnderTest;

            [TestInitialize]
            public void TestSetUp()
            {
                var test = new List<CsvTestClass>();
                test.Add(new CsvTestClass()
                {
                    PropString = "test11",
                    PropMapString = "test12",
                    PropIgnoreString = "test13",
                    PropListString = new List<string>() { "prim11", "prim12" },
                    PropListClass = new List<inner>()
                        {
                            new inner() { Test = "inner11"},
                            new inner() {Test = "inner12"}
                        }
                });

                test.Add(new CsvTestClass()
                {
                    PropString = "test21",
                    PropMapString = "test22",
                    PropIgnoreString = "test23",
                    PropListString = new List<string>() { "prim21", "prim22" },
                    PropListClass = new List<inner>()
                        {
                            new inner() { Test = "inner21"},
                            new inner() {Test = "inner22"}
                        }
                });
                this.ObjectUnderTest = test;
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void NoDataPassedExpectedExcetptionTest()
            {
                var fileWriter = new Mock<FileWriter>();
                var test = new CsvWriter(fileWriter.Object, '.');
                test.Write<string>(null, "test");
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void NoFilePathPassedExpectedExcetptionTest()
            {
                var fileWriter = new Mock<FileWriter>();
                var test = new CsvWriter(fileWriter.Object, '.');
                var data = new List<string>();
                test.Write<string>(data, null);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void EmptyStringRefForFilePathExpectedExcetptionTest()
            {
                var fileWriter = new Mock<FileWriter>();
                var test = new CsvWriter(fileWriter.Object, '.');
                var data = new List<string>();
                test.Write<string>(data, "");
            }

            [TestMethod]
            public void CsvStringGeneratedTest()
            {
                var fileWriter = new Mock<IFileWriter>();
                fileWriter.Setup(m => m.WriteFile(It.IsAny<StringBuilder>(), It.IsAny<string>()));
                var test = new CsvWriter(fileWriter.Object, ',');

                var csvString = new StringBuilder();
                csvString.AppendLine("PropString,MappedName,PropListString,Test");
                csvString.AppendLine(@"test11,test12,""prim11,prim12"",""inner11,inner12""");
                csvString.AppendLine(@"test21,test22,""prim21,prim22"",""inner21,inner22""");
                test.Write(this.ObjectUnderTest, "testPath");

                fileWriter.Verify(m => m.WriteFile(It.IsAny<StringBuilder>(), It.IsAny<string>()));
                fileWriter.Verify(m => m.WriteFile(It.Is<StringBuilder>(s=>
                s.ToString().Equals(csvString.ToString().Trim())
                    ), It.Is<string>(s=> s.Equals("testPath"))));
            }

            //test for empty data passed
            [TestMethod]
            public void EmptyDataSuccessfulCreateDocTest()
            {
                var fileWriter = new Mock<IFileWriter>();
                fileWriter.Setup(m => m.WriteFile(It.IsAny<StringBuilder>(), It.IsAny<string>()));
                var test = new CsvWriter(fileWriter.Object, ',');

                var csvString = new StringBuilder();
                csvString.AppendLine("PropString,MappedName,PropListString,Test");
                test.Write(new List<CsvTestClass>(), "testPath");

                fileWriter.Verify(m => m.WriteFile(It.IsAny<StringBuilder>(), It.IsAny<string>()));
                fileWriter.Verify(m => m.WriteFile(It.Is<StringBuilder>(s =>
                s.ToString().Trim().Equals(csvString.ToString().Trim())
                    ), It.Is<string>(s => s.Equals("testPath"))));
            }
        }
    }

    public class CsvTestClass
    {
        public string PropString { get; set; }

        [CsvMapTo("MappedName")]
        public string PropMapString { get; set; }

        [CsvIgnoreProperty]
        public string PropIgnoreString { get; set; }

        [CsvListProperty(',')]
        public List<string> PropListString { get; set; }

        [CsvListProperty("Test", ',')]
        public List<inner> PropListClass { get; set; }
    }

    public class inner
    {
        public string Test { get; set; }
    }
}
