﻿namespace Tests.Model
{
    using System;
    using System.CodeDom;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Metadata.W3cXsd2001;
    using System.Security.Cryptography.X509Certificates;

    using CsvFileReader.Decorators;
    using CsvFileReader.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    public class TypeMetaDataTest
    {
        [TestClass]
        public class Constructor
        {
            public string TestProperty { get; set; }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void PropertyIsNullThrowExceptionTest()
            {
                var test = new TypeMetaData(null, 2, "test");
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void StirngIsNullThrowExceptionTest()
            {
                var pinfo = this.GetType().GetProperties().First();
                var test = new TypeMetaData(pinfo, 2, null);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void StirngIsWhiteSpaceThrowExceptionTest()
            {
                var pinfo = this.GetType().GetProperties().First();
                var test = new TypeMetaData(pinfo, 2, "");
            }

            [ExpectedException(typeof(ArgumentException))]
            [TestMethod]
            public void intIsGreaterThanOneThrowExceptionTest()
            {
                var pinfo = this.GetType().GetProperties().First();
                var test = new TypeMetaData(pinfo, -1, "test");
            }
        }

        [TestClass]
        public class RetrieveValue
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void ArguemntIsNUllExpectedException()
            {
                var pinfo = new Mock<PropertyInfo>();
                var test = new TypeMetaData(pinfo.Object, 1, "test");
                test.RetrieveValue<Object>(null);
            }

            [TestMethod]
            public void RetrievesNonListValue()
            {
                var tclass = new TestClass() { Test = "test value" };
                var test = new TypeMetaData(tclass.GetType().GetProperty("Test"), 1, "Test");
                var results = test.RetrieveValue(tclass);

                Assert.AreSame("test value", results);
            }

            [TestMethod]
            public void RetrievesListValueForOneItems()
            {
                var propList = new List<TestClass>();
                propList.Add(new TestClass() { Test = "test value" });
                var tclass = new TestClass() { TestList = propList };

                var test = new TypeMetaData(tclass.GetType().GetProperty("TestList"), 1, "Test");
                var results = test.RetrieveValue(tclass);

                Assert.AreEqual(@"""test value""", results);
            }

            [TestMethod]
            public void RetrievesListValueForMultipleItems()
            {
                var propList = new List<TestClass>();
                propList.Add(new TestClass() { Test = "test value" });
                propList.Add(new TestClass() { Test = "other value" });
                var tclass = new TestClass() { TestList = propList };

                var test = new TypeMetaData(tclass.GetType().GetProperty("TestList"), 1, "Test");
                var results = test.RetrieveValue(tclass);

                Assert.AreEqual(@"""test value,other value""", results);
            }

            [TestMethod]
            public void RetrievesListValueForMultiplePrimitives()
            {
                var propList = new List<string>();
                propList.Add("test value" );
                propList.Add("other value");
                var tclass = new TestClass() { TestListPrim = propList };

                var test = new TypeMetaData(tclass.GetType().GetProperty("TestListPrim"), 1, "Test");
                var results = test.RetrieveValue(tclass);

                Assert.AreEqual(@"""test value,other value""", results);
            }
        }

        public class TestClass
        {
            public string Test { get; set; }

            [CsvListProperty("Test", ',')]
            public List<TestClass> TestList { get; set; }
            
            [CsvListProperty(',')]
            public List<String> TestListPrim { get; set; }
        }
    }
}
