﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    /// <summary>
    /// comments class model
    /// </summary>
    public class Comment : ICloneable
    {
        public int ID;
        public String OrgID { get; set; }
        public String Phone { get; set; }
        public String AcctNum { get; set; }

        [CsvDate("MMddyyyy")]
        public DateTime DisDate { get; set; }

        public int SID { get; set; }
        public int LocID { get; set; }
        public String CommentImg { get; set; }
        public String AddressImg { get; set; }
        public int ShowContactInfo { get; set; }


        public String Transcription { get; set; }
        public int Disgruntled { get; set; }
        public int HighPriority { get; set; }

        public int Attitude { get; set; }

        [CsvListProperty("ID", ',')]
        public List<factors> Category { get; set; }
        public int Translated { get; set; }
        public String Transcriptionist { get; set; }
        public String MRNum { get; set; }
        public String CommentText { get; set; }

        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public int Plus4 { get; set; }
        public String DOB { get; set; }


        /// <summary>
        /// clone this current comment and return a new one with the same values
        /// </summary>
        /// <returns></returns>
        public Object Clone()
        {
            Comment clone = new Comment();
            foreach (PropertyInfo info in clone.GetType().GetProperties())
            {
                info.SetValue(clone, info.GetValue(this, null), null);
            }
            foreach (FieldInfo info in clone.GetType().GetFields())
            {
                info.SetValue(clone, info.GetValue(this));
            }
            return clone;
        }
    }
}
