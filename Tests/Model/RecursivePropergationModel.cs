﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class RecursivePropergationModel
    {
        public int OrgID { get; set; }

        [CsvListProperty("OrgID")]
        public RecursivePropergationModel SID { get; set; }
    }
}
