﻿namespace Tests.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;

    using CsvFileReader.Decorators;
    using CsvFileReader.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    public class FileWriterTest
    {
        [TestClass]
        public class WriteValue
        {
            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void ArgumentIsNUllExpectedException()
            {
                var fileWriter = new FileWriter();
                fileWriter.WriteFile(null,It.IsAny<string>());
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void StringArgumentIsNUllExpectedException()
            {
                var fileWriter = new FileWriter();
                fileWriter.WriteFile(new StringBuilder(), null);
            }

            [ExpectedException(typeof(ArgumentNullException))]
            [TestMethod]
            public void StringArgumentIsEmptyExpectedException()
            {
                var fileWriter = new FileWriter();
                fileWriter.WriteFile(new StringBuilder(), "");
            }
            
            [TestMethod]
            public void FileWritesText()
            {
                var fileWriter = new FileWriter();
                fileWriter.WriteFile(new StringBuilder("test"), "NoPath.txt");
                Assert.AreEqual("test",File.ReadAllText("NoPath.txt").Trim());
            }

            [TestCleanup]
            public void CleanUp()
            {
                if(File.Exists("NoPath.txt"))
                    File.Delete("NoPath.txt");
            }
        }

    }
}
