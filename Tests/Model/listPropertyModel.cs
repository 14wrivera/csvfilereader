﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class listPropertyModel
    {
        public int OrgID { get; set; }

        [CsvListProperty("SID", ',')]
        public List<Comment> SID { get; set; }
    }
}
